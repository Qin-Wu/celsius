package sheridan;
/*
 * @author Qin Wu, 991520749
 * Midterm
 */
public class Celsius {

	private static int MIN_F=-460;
	
	public static int fromFahrenheit(int num)throws NumberFormatException {
		
		if (num <MIN_F) {
			throw new NumberFormatException("your degree was wrong!");
		}
		int celsius=(num-32)*5/9;
		
			return celsius;
	
	
	}
}
