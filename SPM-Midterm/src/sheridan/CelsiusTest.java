package sheridan;
/*
 * @author Qin Wu, 991520749
 * Midterm
 */
import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testCelsiusRegular() {
		int result=Celsius.fromFahrenheit(70);
		assertTrue("The degree is correct.",result==21);
	}
	
	@Test
	public void testCelsiusException() {
		int result=Celsius.fromFahrenheit(-1200);
		fail("Invalid degree");
		
	}
	
	@Test
	public void testCelsiusBoundaryIn() {
		int result=Celsius.fromFahrenheit(70);
		assertTrue("The celsius is correct.",result==21);
	}

	@Test
	public void testCelsiusBoundaryOut() {
		int result=Celsius.fromFahrenheit(-460);
		assertFalse("The celsius is correct.",result==-273);
	}
}
